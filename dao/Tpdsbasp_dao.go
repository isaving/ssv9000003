package dao

import (
	"errors"
	"fmt"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/xormplus/xorm"
)

type T_pdsbasp struct {
	PrductId         string  `xorm:"'prduct_id' pk"`
	PrductNm         int     `xorm:"'prduct_nm' pk"`       //` string(5) NOT NULL,
	PrductStatus     string  `xorm:"'prduct_status'"`      //` varchar(1) COLLATE utf8mb4_bin NOT NULL,
	ProductName      string  `xorm:"'product_name'"`       //` varchar(60) COLLATE utf8mb4_bin NOT NULL,
	Currency         string  `xorm:"'currency'"`           //` varchar(3) COLLATE utf8mb4_bin NOT NULL,
	TermFlag         string  `xorm:"'term_flag'"`          //` varchar(1) COLLATE utf8mb4_bin NOT NULL,
	PrductBuyTime    int     `xorm:"'prduct_buy_time'"`    //` int(5) DEFAULT NULL,
	BlncRflctsDrcton string  `xorm:"'blnc_rflcts_drcton'"` //` varchar(1) COLLATE utf8mb4_bin DEFAULT NULL,
	AmtOpenMin       float64 `xorm:"'amt_open_Min'"`       //` decimal(18,2) DEFAULT NULL,
	DepositNature    string  `xorm:"'deposit_Nature'"`     //` varchar(3) COLLATE utf8mb4_bin DEFAULT NULL,
	ApprvlMark       string  `xorm:"'apprvl_mark'"`        //` varchar(1) COLLATE utf8mb4_bin DEFAULT NULL,
	WdrwlMthd        string  `xorm:"'wdrwl_mthd'"`         //` varchar(1) COLLATE utf8mb4_bin DEFAULT NULL,
	AppntDtFlg       string  `xorm:"'appnt_dt_flg'"`       //` varchar(1) COLLATE utf8mb4_bin DEFAULT NULL,
	DepcreFlag       string  `xorm:"'depcre_flag'"`        //` varchar(1) COLLATE utf8mb4_bin DEFAULT NULL,
	SttmntFlg        string  `xorm:"'sttmnt_flg'"`         //` varchar(1) COLLATE utf8mb4_bin DEFAULT NULL,
	SttmntType       string  `xorm:"'sttmnt_type'"`        //` varchar(1) COLLATE utf8mb4_bin DEFAULT NULL,
	SttmntPeriod     string  `xorm:"'sttmnt_period'"`      //` varchar(1) COLLATE utf8mb4_bin DEFAULT NULL,
	SttmntFrequency  int     `xorm:"'sttmnt_frequency'"`   //` int(5) DEFAULT NULL,
	SttmntDate       int     `xorm:"'sttmnt_date'"`        //` int(2) DEFAULT NULL,
	SlpTrnChange     string  `xorm:"'slp_trn_change'"`     //` varchar(1) COLLATE utf8mb4_bin DEFAULT NULL,
	StpTrnAut        string  `xorm:"'stp_trn_aut'"`        //` varchar(1) COLLATE utf8mb4_bin DEFAULT NULL,
	StpAmtMin        float64 `xorm:"'stp_amt_min'"`        //` decimal(18,2) DEFAULT NULL,
	StpGraceDate     int     `xorm:"'stp_grace_date'"`     //` int(3) DEFAULT NULL,
	StpNotDate       int     `xorm:"'stp_not_date'"`       //` int(3) DEFAULT NULL,
	StpIntFlag       string  `xorm:"'stp_int_flag'"`       //` varchar(1) COLLATE utf8mb4_bin DEFAULT NULL,
	StpUnclaimedDay  int     `xorm:"'stp_unclaimed_day'"`  //` int(5) DEFAULT NULL,
	CancelAutoFlag   string  `xorm:"'cancel_auto_flag'"`   //` varchar(1) COLLATE utf8mb4_bin DEFAULT NULL,
	CancelAutoDay    int     `xorm:"'cancel_auto_day'"`    //` int(5) DEFAULT NULL,
	CancelRepnFlag   string  `xorm:"'cancel_repn_flag'"`   //` varchar(1) COLLATE utf8mb4_bin DEFAULT NULL,
	TccStatus        int     `xorm:"'tcc_status'"`         //` int(1) DEFAULT NULL,
}

func (T_pdsbasp) TableName() string {
	return "t_pdsbasp"
}

func QueryT_pdsbaspByPStatus(PrductStatus string, o *xorm.Engine) (t_pdsbasp []*T_pdsbasp, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_pdsbasp panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	//QueryTpdsbaspByPStatus := []*T_pdsbasp{}
	if err := o.Where("tcc_status=? and prduct_status=? ", 0, PrductStatus).Find(&t_pdsbasp); err != nil {
		log.Info("Find T_pdsbasp 错误!")
		return nil, err
	}
	return t_pdsbasp, nil
}
