package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACRLPII struct {
	PeriodNum			string
	LoanAcctgAcctNo		string
}

type DAACRLPIO struct {
	LogTotCount string    `json:"LogTotCount"`
	Records     []DAACRLPIORecords `json:"Records"`
}
type DAACRLPIORecords struct {
	Balance         interface{} `json:"Balance"`
	BalanceType     interface{} `json:"BalanceType"`
	Interest        string      `json:"Interest"`
	LoanAcctgAcctNo string      `json:"LoanAcctgAcctNo"`
	PeriodNum       int         `json:"PeriodNum"`
	PrviousRateDate interface{} `json:"PrviousRateDate"`
	StartRateDate   interface{} `json:"StartRateDate"`
	Status          interface{} `json:"Status"`
}

type DAACRLPIIDataForm struct {
	FormHead CommonFormHead
	FormData DAACRLPII
}

type DAACRLPIODataForm struct {
	FormHead CommonFormHead
	FormData DAACRLPIO
}

type DAACRLPIRequestForm struct {
	Form []DAACRLPIIDataForm
}

type DAACRLPIResponseForm struct {
	Form []DAACRLPIODataForm
}

// @Desc Build request message
func (o *DAACRLPIRequestForm) PackRequest(DAACRLPII DAACRLPII) (responseBody []byte, err error) {

	requestForm := DAACRLPIRequestForm{
		Form: []DAACRLPIIDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRLPII",
				},
				FormData: DAACRLPII,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACRLPIRequestForm) UnPackRequest(request []byte) (DAACRLPII, error) {
	DAACRLPII := DAACRLPII{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACRLPII, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRLPII, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACRLPIResponseForm) PackResponse(DAACRLPIO DAACRLPIO) (responseBody []byte, err error) {
	responseForm := DAACRLPIResponseForm{
		Form: []DAACRLPIODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRLPIO",
				},
				FormData: DAACRLPIO,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACRLPIResponseForm) UnPackResponse(request []byte) (DAACRLPIO, error) {

	DAACRLPIO := DAACRLPIO{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACRLPIO, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRLPIO, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACRLPII) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
