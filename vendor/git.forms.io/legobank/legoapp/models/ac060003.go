package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type AC060003I struct {
	AcctgAcctNo string `valid:"Required;MaxSize(20)"` //贷款核算账号
	DealDate    string //交易日期
	DealTime    string //交易时间
	SponsorChnl string //发起渠道
	JoinupChnl  string //接入渠道
	DealTell    string //交易柜员
}

type AC060003O struct {
	AcctCreateDate     string
	AcctgAcctNo        string
	AcctgStatus        string
	AcctingOrgId       string
	BalanceType        string
	Currency           string
	FrzAmt             float64
	HoliCalcIntFlag    string
	IntCyc             string
	IntFreq            int
	IntSetlCyc         string
	IntSetlFreq        int
	IntSetlPreFlag     string
	IntSetlSpeDate     string
	IntSpeDate         string
	IsCalcDupInt       string
	IsCalcInt          string
	LastCalcIntDate    string
	LastMaintBrno      string
	LastMaintDate      string
	LastMaintTell      string
	LastMaintTime      string
	LastTranDate       string
	LastTranIntDate    string
	LastWdIntDate      string
	MgmtOrgId          string
	OrgCreate          string
	PmitBkvtFlag       string
	PmitDlydBegintFlag string
	Records            []AC060003ORecords
}

type AC060003ORecords struct {
	AcctgAcctNo   string
	PrinStatus    string
	IntPlanNo     string
	OrgCreate     string
	ValidFlag     string
	Bk1           string
	Bk2           string
	Bk3           string
	LastMaintDate string
	LastMaintTime string
	LastMaintBrno string
	LastMaintTell string

}


type AC060003IDataForm struct {
	FormHead CommonFormHead
	FormData AC060003I
}

type AC060003ODataForm struct {
	FormHead CommonFormHead
	FormData AC060003O
}

type AC060003RequestForm struct {
	Form []AC060003IDataForm
}

type AC060003ResponseForm struct {
	Form []AC060003ODataForm
}

// @Desc Build request message
func (o *AC060003RequestForm) PackRequest(AC060003I AC060003I) (responseBody []byte, err error) {

	requestForm := AC060003RequestForm{
		Form: []AC060003IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC060003I",
				},
				FormData: AC060003I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *AC060003RequestForm) UnPackRequest(request []byte) (AC060003I, error) {
	AC060003I := AC060003I{}
	if err := json.Unmarshal(request, o); nil != err {
		return AC060003I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC060003I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *AC060003ResponseForm) PackResponse(AC060003O AC060003O) (responseBody []byte, err error) {
	responseForm := AC060003ResponseForm{
		Form: []AC060003ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC060003O",
				},
				FormData: AC060003O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *AC060003ResponseForm) UnPackResponse(request []byte) (AC060003O, error) {

	AC060003O := AC060003O{}

	if err := json.Unmarshal(request, o); nil != err {
		return AC060003O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC060003O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *AC060003I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
