package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACCLD0I struct {
	TxAcctnDt                 string
	TxTm                      string
	HostTranSerialNo          string ` validate:"required,max=34"`
	HostTranSeq               string ` validate:"required,max=34"`
	HostTranInq               string ` validate:"required,max=6"`
	BookedWorkday             string
	AcctingTime               string
	LiquidationDate           string
	LiquidationTime           string
	TranOrgId                 string
	AgentOrgId                string
	TranTeller                string
	AuthTeller                string
	ReviewTeller              string
	TranChannel               string
	AccessChannel             string
	TerminalNo                string
	BussSys                   string
	TrasactionCode            string
	ClearingBussType          string
	BussType                  string
	BussCategories            string
	DebitFlag                 string
	LocalBankFlag             string
	Country                   string
	OtherBankFlag             string
	AcctingOrgId              string
	LocalAcctDiff             string
	LocalTranDetailType       string
	CustId                    string
	TheMerchantNo             string
	BussAcctNo                string
	AcctNo                    string
	AcctgAcctNo               string
	AmtType                   string
	Currency                  string
	BanknoteFlag              string
	Amt                       float64
	AcctBal                   float64
	CashTranFlag              string
	ValueDate                 string
	ExchRestType              string
	AntiTradingMark           string
	CorrectTradingFlag        string
	OriginalTradingDay        string
	OriginalHostTranSerialNo  string
	OriginalTradeTellerNo     string
	AcctManagerNo             string
	MessageCode               string
	AbstractCode              string
	UseCode                   string
	SetNo                     string
	WhetherToAllowPunching    string
	ChannelCorrectControlFlag string
	IsSendClearingFlag        string
	IsMakeUpForPayables       string
	LastMaintDate             string
	LastMaintTime             string
	LastMaintBrno             string
	LastMaintTell             string
	TccState                  int8
	Abstract                  string
	Status                    int64
}

type DAACCLD0O struct {
	TxAcctnDt                 string
	TxTm                      string
	HostTranSerialNo          string
	HostTranSeq               string
	HostTranInq               string
	BookedWorkday             string
	AcctingTime               string
	LiquidationDate           string
	LiquidationTime           string
	TranOrgId                 string
	AgentOrgId                string
	TranTeller                string
	AuthTeller                string
	ReviewTeller              string
	TranChannel               string
	AccessChannel             string
	TerminalNo                string
	BussSys                   string
	TrasactionCode            string
	ClearingBussType          string
	BussType                  string
	BussCategories            string
	DebitFlag                 string
	LocalBankFlag             string
	Country                   string
	OtherBankFlag             string
	AcctingOrgId              string
	LocalAcctDiff             string
	LocalTranDetailType       string
	CustId                    string
	TheMerchantNo             string
	BussAcctNo                string
	AcctNo                    string
	AcctgAcctNo               string
	AmtType                   string
	Currency                  string
	BanknoteFlag              string
	Amt                       float64
	AcctBal                   float64
	CashTranFlag              string
	ValueDate                 string
	ExchRestType              string
	AntiTradingMark           string
	CorrectTradingFlag        string
	OriginalTradingDay        string
	OriginalHostTranSerialNo  string
	OriginalTradeTellerNo     string
	AcctManagerNo             string
	MessageCode               string
	AbstractCode              string
	UseCode                   string
	SetNo                     string
	WhetherToAllowPunching    string
	ChannelCorrectControlFlag string
	IsSendClearingFlag        string
	IsMakeUpForPayables       string
	LastMaintDate             string
	LastMaintTime             string
	LastMaintBrno             string
	LastMaintTell             string
	TccState                  int8
	Abstract                  string
	Status                    int64
}

type DAACCLD0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACCLD0I
}

type DAACCLD0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACCLD0O
}

type DAACCLD0RequestForm struct {
	Form []DAACCLD0IDataForm
}

type DAACCLD0ResponseForm struct {
	Form []DAACCLD0ODataForm
}

// @Desc Build request message
func (o *DAACCLD0RequestForm) PackRequest(DAACCLD0I DAACCLD0I) (responseBody []byte, err error) {

	requestForm := DAACCLD0RequestForm{
		Form: []DAACCLD0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCLD0I",
				},
				FormData: DAACCLD0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACCLD0RequestForm) UnPackRequest(request []byte) (DAACCLD0I, error) {
	DAACCLD0I := DAACCLD0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACCLD0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCLD0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACCLD0ResponseForm) PackResponse(DAACCLD0O DAACCLD0O) (responseBody []byte, err error) {
	responseForm := DAACCLD0ResponseForm{
		Form: []DAACCLD0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCLD0O",
				},
				FormData: DAACCLD0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACCLD0ResponseForm) UnPackResponse(request []byte) (DAACCLD0O, error) {

	DAACCLD0O := DAACCLD0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACCLD0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCLD0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACCLD0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
