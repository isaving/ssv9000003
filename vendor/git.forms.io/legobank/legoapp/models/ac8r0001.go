package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type AC8R0001I struct {
	AcctgAcctNo	string
	PeriodNum	int
	Flag		string
}

type AC8R0001O struct {
	PageTotCount	int
	PageNo			int
	Records			[]AC8R0001ORecords
}

type AC8R0001ORecords struct {
	AcctgAcctNo    string
	Currency       string
	LoanStatus     string
	AcctType       string
	OrgCreate      string
	AcctCreateDate string
	CurrIntStDate  string
	PeriodNum      int64
	BalanceType    string
	Balance        float64
	CavAmt         float64
	LastTranDate   string
	MgmtOrgId      string
	AcctingOrgId   string
	LastMaintDate  string
	LastMaintTime  string
	LastMaintBrno  string
	LastMaintTell  string
}


type AC8R0001IDataForm struct {
	FormHead CommonFormHead
	FormData AC8R0001I
}

type AC8R0001ODataForm struct {
	FormHead CommonFormHead
	FormData AC8R0001O
}

type AC8R0001RequestForm struct {
	Form []AC8R0001IDataForm
}

type AC8R0001ResponseForm struct {
	Form []AC8R0001ODataForm
}

// @Desc Build request message
func (o *AC8R0001RequestForm) PackRequest(AC8R0001I AC8R0001I) (responseBody []byte, err error) {

	requestForm := AC8R0001RequestForm{
		Form: []AC8R0001IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC8R0001I",
				},
				FormData: AC8R0001I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *AC8R0001RequestForm) UnPackRequest(request []byte) (AC8R0001I, error) {
	AC8R0001I := AC8R0001I{}
	if err := json.Unmarshal(request, o); nil != err {
		return AC8R0001I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC8R0001I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *AC8R0001ResponseForm) PackResponse(AC8R0001O AC8R0001O) (responseBody []byte, err error) {
	responseForm := AC8R0001ResponseForm{
		Form: []AC8R0001ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC8R0001O",
				},
				FormData: AC8R0001O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *AC8R0001ResponseForm) UnPackResponse(request []byte) (AC8R0001O, error) {

	AC8R0001O := AC8R0001O{}

	if err := json.Unmarshal(request, o); nil != err {
		return AC8R0001O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC8R0001O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *AC8R0001I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
