//Version: 1.0.0
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type EPD20002I struct {
	Barcode string `json:"barcode"`
}

type EPD20002O struct {
	ProductId         string   `json:"productId"`
	ProductName       string   `json:"productName"`
	BrandName         string   `json:"brandName"`
	QuantityUom       string   `json:"quantityUom"`
	IsVirtual         bool     `json:"isVirtual"`
	Taxable           bool     `json:"taxable"`
	ProductPromotions []string `json:"productPromotions"`
	PromotionId       string   `json:"promotionId"`
	PromotionName     string   `json:"promotionName"`
	ProductSkus       []string `json:"productSkus"`
	ProductSkuId      string   `json:"productSkuId"`
	ProductSkuName    string   `json:"productSkuName"`
	ProductImgUrls    []string `json:"productImgUrls"`
	ProductFeatures   []string `json:"productFeatures"`
}

// @Desc Build request message
func (o *EPD20002I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *EPD20002I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *EPD20002O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *EPD20002O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *EPD20002I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*EPD20002I) GetServiceKey() string {
	return "epd20002"
}
