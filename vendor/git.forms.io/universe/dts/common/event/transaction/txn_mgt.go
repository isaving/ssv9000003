//Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.

package event

// root transaction register request
type RootTxnRegisterRequestBody struct {
	ParticipantAddress string `json:"participantAddress"`
	RequestTime        string `json:"requestTime"`
	ParentXid          string `json:"parentXid"`
	RootXid            string `json:"rootXid"`
	BranchXid          string `json:"branchXid"`
}
type RootTxnRegisterRequest struct {
	Head    TxnEventHeader             `json:"head"`
	Request RootTxnRegisterRequestBody `json:"request"`
}

// root transaction register response
type RootTxnRegisterResponseBody struct {
	RootXid      string `json:"rootXid"`
	ResponseTime string `json:"responseTime"`
}

type RootTxnRegisterResponse struct {
	ErrorCode int                         `json:"errorCode"`
	ErrorMsg  string                      `json:"errorMsg"`
	Data      RootTxnRegisterResponseBody `json:"data"`
}

// branch transaction enlist request
type BranchTxnEnlistRequestBody struct {
	ParticipantAddress string `json:"participantAddress"`
	RequestTime        string `json:"requestTime"`
	ParentXid          string `json:"parentXid"`
	RootXid            string `json:"rootXid"`
	BranchXid          string `json:"branchXid"`
}

type BranchTxnEnlistRequest struct {
	Head    TxnEventHeader             `json:"head"`
	Request BranchTxnEnlistRequestBody `json:"request"`
}

// branch transaction enlist response
type BranchTxnEnlistResponseBody struct {
	BranchXid    string `json:"branchXid"`
	ResponseTime string `json:"responseTime"`
}

type BranchTxnEnlistResponse struct {
	ErrorCode int                         `json:"errorCode"`
	ErrorMsg  string                      `json:"errorMsg"`
	Data      BranchTxnEnlistResponseBody `json:data`
}

// transaction try result report request
type TxnTryResultReportRequestBody struct {
	RootXid     string `json:"rootXid"`
	BranchXid   string `json:"branchXid"`
	ParentXid   string `json:"parentXid"`
	Ok          bool   `json:"ok"`
	RequestTime string `json:"requestTime"`
}

type TxnTryResultReportRequest struct {
	Head    TxnEventHeader                `json:"head"`
	Request TxnTryResultReportRequestBody `json:"request"`
}

type TxnTryResultReportResponseBody struct {
	ResponseTime string `json:"responseTime"`
}

type TxnTryResultReportResponse struct {
	ErrorCode int                            `json:"errorCode"`
	ErrorMsg  string                         `json:"errorMsg"`
	Data      TxnTryResultReportResponseBody `json:"data"`
}

// abnormal transaction processing request
type AbnormalTxnProcessingRequestBody struct {
	RootXid     string `json:"rootXid"`
	BranchXid   string `json:"branchXid"`
	Operation   string `json:"operation"`
	RequestTime string `json:"requestTime"`
}

type AbnormalTxnProcessingRequest struct {
	Head    TxnEventHeader                   `json:"head"`
	Request AbnormalTxnProcessingRequestBody `json:"request"`
}

type AbnormalTxnProcessingResponseBody struct {
	ResponseTime string `json:"responseTime"`
}
