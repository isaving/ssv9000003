//
// Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.
//

package crypto

type CryptoInterface interface {
	Encrypt(plainText []byte) ([]byte, error)
	Decrypt(cipherText []byte) ([]byte, error)
}

type SignInterface interface {
	Sign(data []byte) ([]byte, error)
	VerifySign(data, signData []byte) (bool, error)
}
