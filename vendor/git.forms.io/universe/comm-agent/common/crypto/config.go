//
// Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.
//

package crypto

import (
	"git.forms.io/universe/comm-agent/common/config"
	"github.com/go-errors/errors"
	"sync"
	"time"
)

// CryptoCfg stores a list of parameters related to the encryption and decryption of all
// upstream and downstream services of the current service and the current service.
var cryptoCfg *CryptoConfig
var lock sync.RWMutex

// GetServiceKey provides the caller to obtain the crypto key of service
// through the service id.
func GetServiceKey(serviceId string) (*ServiceKey, bool, error) {
	if nil == cryptoCfg {
		return nil, false, errors.Errorf("Cannot found key with service id:[%s] ", serviceId)
	}

	// 1.Get all sorted key arrays according to service Id
	scc, ok := cryptoCfg.ServiceCryptoConfigMap[serviceId]
	if !ok {
		return nil, false, errors.Errorf("Cannot found service crypto key with service id[%s] ", serviceId)
	}

	// 2.Determine if the current version is invalid
	// Use the new key 60 seconds in advance
	if scc.CurrentKeyExpireDate.Before(time.Now().Add(time.Duration(config.CommAgentCfg.Crypto.PeriodOfKeyUpgradeCheck+60) * time.Second)) {

		// 2.1 If the current version has expired
		//     determine if the next version key exists.
		if scc.HasNextKey {

			// 3.1 Exist, determine if the next version is in effect
			if scc.SortedServiceKeyArray[scc.CurrentKeyIndex+1].ActiveDate.Before(time.Now()) {

				// 3.1.1 In effect, the new version is used and the current version key information is replaced.
				scc.CurrentKeyIndex = scc.CurrentKeyIndex + 1
				scc.CurrentKeyExpireDate = scc.SortedServiceKeyArray[scc.CurrentKeyIndex].ExpireDate

				scc.HasNextKey = scc.CurrentKeyIndex < len(scc.SortedServiceKeyArray)
				return scc.SortedServiceKeyArray[scc.CurrentKeyIndex], scc.HasNextKey, nil
			} else {

				//return nil, fmt.Errorf("no active Key with service:[%s]", serviceId)
				return nil, false, errors.Errorf("no active Key with service:[%s]", serviceId)
			}
		}
	} else {

		// 2.2 If the current version does not expire, the current version of the key is used.
		return scc.SortedServiceKeyArray[scc.CurrentKeyIndex], scc.HasNextKey, nil
	}

	//log.Errorf("Cannot found available crypto key with service:[%s]!", serviceId)
	return nil, false, errors.Errorf("Cannot found available crypto key with service:[%s]!", serviceId)
}

// GetServiceKeyWithVersion provides the caller to obtain the crypto key of service
// through the service id and version number.
func GetServiceKeyWithVersion(serviceId string, version string) (*ServiceKey, error) {
	if nil == cryptoCfg || nil == cryptoCfg.ServiceWithKeyVersionMap {
		//return nil, fmt.Errorf("Cannot found service crypto key with service id[%s]-version[%s] ", serviceId, version)
		return nil, errors.Errorf("Cannot found service crypto key with service id[%s]-version[%s] ", serviceId, version)
	}
	if svm, ok := cryptoCfg.ServiceWithKeyVersionMap[serviceId+version]; !ok {
		//return nil, fmt.Errorf("Cannot found service crypto key with service id[%s]-version[%s] ", serviceId, version)
		return nil, errors.Errorf("Cannot found service crypto key with service id[%s]-version[%s] ", serviceId, version)
	} else {
		return svm, nil
	}
}

// DateFormatter defines the date formatter of active date and expire date of service key
var DateFormatter = "20060102"

//CryptoConfig stores the keys of all upstream and downstream service lists
type CryptoConfig struct {
	ServiceCryptoConfigMap   map[string]*ServiceCryptoConfig
	ServiceWithKeyVersionMap map[string]*ServiceKey
}

// ServiceCryptoConfig stores the keys of all upstream and downstream service lists
type ServiceCryptoConfig struct {
	ServiceId             string
	CryptoMode            string
	Padding               string
	CurrentKeyExpireDate  time.Time
	CurrentKeyIndex       int
	HasNextKey            bool
	SortedServiceKeyArray []*ServiceKey
}

// ServiceKey stores key、version of key、active date of key、expire date of key
type ServiceKey struct {
	Version    int
	Key        []byte
	ActiveDate time.Time
	ExpireDate time.Time
}

func UpdateCryptoCfg(cc *CryptoConfig) error {
	lock.Lock()
	cryptoCfg = cc
	lock.Unlock()
	return nil
}

func ReplaceCurrentServiceCryptoCfg(cc *CryptoConfig) error {
	lock.Lock()

	for k, v := range cc.ServiceCryptoConfigMap {
		delete(cryptoCfg.ServiceCryptoConfigMap, k)
		cryptoCfg.ServiceCryptoConfigMap[k] = v
	}

	for k, v := range cc.ServiceWithKeyVersionMap {
		delete(cryptoCfg.ServiceWithKeyVersionMap, k)
		cryptoCfg.ServiceWithKeyVersionMap[k] = v
	}
	lock.Unlock()
	return nil
}

func GetCryptoCfg() *CryptoConfig {
	return cryptoCfg
}
