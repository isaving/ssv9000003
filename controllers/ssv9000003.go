//Version: v0.0.1
package controllers

import (
	"git.forms.io/isaving/sv/ssv9000003/dao"
	"git.forms.io/isaving/sv/ssv9000003/models"
	"git.forms.io/isaving/sv/ssv9000003/services"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/controllers"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/solapp-sdk/log"
	"runtime/debug"
)

type Ssv9000003Controller struct {
	controllers.CommController
}

func (*Ssv9000003Controller) ControllerName() string {
	return "Ssv9000003Controller"
}

// @Desc ssv9000003 controller
// @Description Entry
// @Param ssv9000003 body models.SSV9000003I true "body for user content"
// @Success 200 {object} models.SSV9000003O
// @router /ssv9000003 [post]
// @Author
// @Date 2020-12-04
func (c *Ssv9000003Controller) Ssv9000003() {

	defer func() {
		if r := recover(); r != nil {
			err := errors.Errorf(constant.SYSPANIC, "Ssv9000003Controller.Ssv9000003 Catch panic %v", r)
			log.Errorf("Error: %v, Stack: [%s]", err, debug.Stack())
			c.SetServiceError(err)
		}
	}()

	ssv9000003I := &models.SSV9000003I{}
	if err := models.UnPackRequest(c.Req.Body, ssv9000003I); err != nil {
		c.SetServiceError(err)
		return
	}
	if err := ssv9000003I.Validate(); err != nil {
		log.Errorf("Request message field validate failed, error:[%v]", err)
		c.SetServiceError(err)
		return
	}
	ssv9000003 := &services.Ssv9000003Impl{}
	ssv9000003.New(c.CommController)
	ssv9000003.Sv900003I = ssv9000003I
	ssv9000003.O=dao.EngineCache
	ssv9000003O, err := ssv9000003.Ssv9000003(ssv9000003I)

	if err != nil {
		log.Errorf("Ssv9000003Controller.Ssv9000003 failed, err=%v", errors.ServiceErrorToString(err))
		c.SetServiceError(err)
		return
	}
	responseBody, err := ssv9000003O.PackResponse()
	if err != nil {
		c.SetServiceError(err)
		return
	}
	c.SetAppBody(responseBody)
}

// @Title Ssv9000003 Controller
// @Description ssv9000003 controller
// @Param Ssv9000003 body models.SSV9000003I true body for SSV9000003 content
// @Success 200 {object} models.SSV9000003O
// @router /create [post]
/*func (c *Ssv9000003Controller) SWSsv9000003() {
	//Here is to generate API documentation, no need to implement methods
}
*/