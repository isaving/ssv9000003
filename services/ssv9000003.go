//Version: v0.0.1
package services

import (
	"git.forms.io/isaving/sv/ssv9000003/dao"
	"git.forms.io/isaving/sv/ssv9000003/models"
	"git.forms.io/legobank/legoapp/services"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/xormplus/xorm"
)

type Ssv9000003Impl struct {
	services.CommonService
	//TODO ADD Service Self Define Field
	Sv900003O      *models.SSV9000003O
	Sv900003I      *models.SSV9000003I
	CommonResponse *models.CommonResponse
	O              *xorm.Engine
}

// @Desc Ssv9000003 process
// @Author
// @Date 2020-12-04
func (impl *Ssv9000003Impl) Ssv9000003(ssv9000003I *models.SSV9000003I) (ssv9000003O *models.SSV9000003O, err error) {

	impl.Sv900003I = ssv9000003I

	//TODO Service Business Process
	log.Debug("接口获取的值是：", impl.Sv900003I.PrductStatus)
	//通过产品状态，查询产品基础信息表 返回多条数据
	QueryT_pdsbaspByPStatus, err := dao.QueryT_pdsbaspByPStatus(impl.Sv900003I.PrductStatus, impl.O)
	if err != nil {
		log.Info("server Find T_pdsbasp 错误!")
		return nil, err
	}
	if len(QueryT_pdsbaspByPStatus) == 0 {
		log.Info("查询没有数据!")
		return nil, nil
	}
	log.Debug("查询出来的长度是：", len(QueryT_pdsbaspByPStatus))
	log.Debug("查询出来的值是：", QueryT_pdsbaspByPStatus)
	sv90003Records := []models.SSV9000003ORecord{}
	for k, _ := range QueryT_pdsbaspByPStatus {
		SSV9000003ORecord := models.SSV9000003ORecord{
			PrductId:            QueryT_pdsbaspByPStatus[k].PrductId,
			PrductNm:            QueryT_pdsbaspByPStatus[k].PrductNm,
			ProductNameCh:       QueryT_pdsbaspByPStatus[k].ProductName,
			ProductNameEn:       QueryT_pdsbaspByPStatus[k].ProductName,
			Currency:            QueryT_pdsbaspByPStatus[k].Currency,
			OffShoreOnshoreSign: "",
			TermFlag:            QueryT_pdsbaspByPStatus[k].TermFlag,
			DepositPeriodUnit:   "",
			DepositPeriod:       0,
		}
		sv90003Records = append(sv90003Records, SSV9000003ORecord)
	}
	ssv9000003O = &models.SSV9000003O{
		//TODO Assign  value to the OUTPUT Struct field
		RecordTotNum: len(QueryT_pdsbaspByPStatus),
		Records:      sv90003Records,
	}
	return ssv9000003O, nil
}
