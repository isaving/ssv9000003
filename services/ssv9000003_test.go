//Version: 1.0.0
package services

import (
	"errors"
	"git.forms.io/isaving/sv/ssv9000003/dao"
	"git.forms.io/isaving/sv/ssv9000003/models"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/astaxie/beego"
	. "github.com/smartystreets/goconvey/convey"
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	_ = beego.LoadAppConfig("ini", "../conf/app.conf")
	_ = dao.InitDatabase()
	exitVal := m.Run()
	os.Exit(exitVal)
}

func TestEul20001Impl_Eul20001_Success(t *testing.T) {
	input := &models.SSV9000003I{
		PrductStatus: "1",
	}

	impl := Ssv9000003Impl{
		Sv900003I: input,
		O:         dao.EngineCache,
	}

	Convey("Call service", t, func() {
		//var session *dao.Session
		Convey("Mock data", func() {
			db, mock, err := sqlmock.New()
			So(err, ShouldBeNil)
			dao.EngineCache.DB().DB = db
			mock.ExpectQuery("t_pdsbasp").
				WillReturnRows(sqlmock.NewRows([]string{"prduct_id"}).AddRow("1000001"))
			Convey("Validate session", func() {
				_, _ = impl.Ssv9000003(input)
			})
		})

	})
}

func TestEul20001Impl_Eul20001_Flase(t *testing.T) {
	input := &models.SSV9000003I{
		PrductStatus: "1",
	}

	impl := Ssv9000003Impl{
		Sv900003I: input,
		O:         dao.EngineCache,
	}

	Convey("Call service", t, func() {
		//var session *dao.Session
		Convey("Mock data", func() {
			db, mock, err := sqlmock.New()
			So(err, ShouldBeNil)
			dao.EngineCache.DB().DB = db
			mock.ExpectQuery("t_pdsbasp").WillReturnError(errors.New("错误"))
			Convey("Validate session", func() {
				_, _ = impl.Ssv9000003(input)
			})
		})

	})
}


